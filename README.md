## PostgreSQL client proxy repository for hell

This is proxy repository for [libpq library](https://www.postgresql.org/), which allow you to build and install it  using [hell dependency manager](https://gitlab.com/rilis/hell/hell).

* If you have problem with installation of libpg using hell, have improvement idea, or want to request support for other versions of Postgresql, then please [create issue here](https://gitlab.com/rilis/rilis/issues).
* If you found bug in libpg itself please create issue on [PostgreSQL support page](https://www.postgresql.org/), because here we don't do any kind of PostgreSQL development.
